#include "project.h"

static int shmem_fd;
static void *shmem_ptr;
static size_t shmem_sz;
static pid_t children_pid[4] = {0, 0, 0, 0};

static void signal_handler1(int i) { (void)i; exit(1); }
static void signal_handler2(int i) { (void)i; _Exit(2); }
static void dtor_shm(void) { shm_unlink("/soi_shm"); }
static void dtor_map(void) { munmap(shmem_ptr, shmem_sz); }
static void dtor_sem_a(void) { sem_unlink("/soi_sem_access"); }
static void dtor_sem_f(void) { sem_unlink("/soi_sem_filled"); }
static void dtor_sem_e(void) { sem_unlink("/soi_sem_empty"); }
static void dtor_sem_r(void) { sem_unlink("/soi_sem_read"); }
static void dtor_sem_m1(void) { sem_unlink("/soi_sem_mark1"); }
static void dtor_sem_m2(void) { sem_unlink("/soi_sem_mark2"); }
static void dtor_children(void)
{
    for (int j = 0; j < 4; ++j)
        if (children_pid[j] != 0) kill(children_pid[j], SIGTERM);
}

soi_bufinfo_t *bufinfo;
soi_item_t *buffer;

int main(void)
{
    // Allocate resources
    shmem_sz = sizeof(soi_bufinfo_t) + N * sizeof(soi_item_t);

    shmem_fd = shm_open("/soi_shm", O_CREAT | O_EXCL | O_RDWR, S_IRUSR | S_IWUSR);
    if (shmem_fd == -1) exit(5);
    atexit(dtor_shm);

    int r = ftruncate(shmem_fd, shmem_sz);
    if (r != 0) exit(7);

    shmem_ptr = mmap(NULL, shmem_sz, PROT_READ | PROT_WRITE, MAP_SHARED, shmem_fd, 0);
    if (shmem_ptr == MAP_FAILED) exit(9);
    atexit(dtor_map);

    bufinfo = (soi_bufinfo_t *)shmem_ptr;
    bufinfo->position = 0;
    bufinfo->size = 0;
    bufinfo->capacity = N;
    buffer = (soi_item_t *)((void *)bufinfo + sizeof(soi_bufinfo_t));

    bufinfo->s_access = sem_open("/soi_sem_access", O_CREAT | O_EXCL, S_IRUSR | S_IWUSR, 1);
    if (bufinfo->s_access == SEM_FAILED) exit(21);
    atexit(dtor_sem_a);

    bufinfo->s_filled = sem_open("/soi_sem_filled", O_CREAT | O_EXCL, S_IRUSR | S_IWUSR, 0);
    if (bufinfo->s_filled == SEM_FAILED) exit(22);
    atexit(dtor_sem_f);

    bufinfo->s_empty = sem_open("/soi_sem_empty", O_CREAT | O_EXCL, S_IRUSR | S_IWUSR, N);
    if (bufinfo->s_empty == SEM_FAILED) exit(23);
    atexit(dtor_sem_e);

    bufinfo->s_read = sem_open("/soi_sem_read", O_CREAT | O_EXCL, S_IRUSR | S_IWUSR, 2);
    if (bufinfo->s_read == SEM_FAILED) exit(24);
    atexit(dtor_sem_r);

    bufinfo->s_mark1 = sem_open("/soi_sem_mark1", O_CREAT | O_EXCL, S_IRUSR | S_IWUSR, 1);
    if (bufinfo->s_mark1 == SEM_FAILED) exit(25);
    atexit(dtor_sem_m1);

    bufinfo->s_mark2 = sem_open("/soi_sem_mark2", O_CREAT | O_EXCL, S_IRUSR | S_IWUSR, 1);
    if (bufinfo->s_mark2 == SEM_FAILED) exit(26);
    atexit(dtor_sem_m2);

    atexit(dtor_children);
    signal(SIGINT, signal_handler1);

    // Spawn sub-processes
    for (int i = 0; i < 4; ++i)
    {
        pid_t p = fork();

        if (p == -1)
        {
            // Fork failed, exit (will kill children)
            exit(33);
        }
        else if (p == 0)
        {
            // Default handler
            signal(SIGINT, signal_handler2);
            switch (i)
            {
            case 0:
                producer();
                break;
            case 1:
                consumer();
                break;
            case 2:
                reader1();
                break;
            case 3:
                reader2();
                break;
            default:
                break;
            }

            // Unreachable
            _Exit(0);
        }
        else // p > 0
        {
            children_pid[i] = p;
        }
    }

    for (int i = 0; i < 4; ++i)
    {
        int status;
        waitpid(children_pid[i], &status, 0);
        children_pid[i] = 0;
    }

    // Execute destructors
    exit(0);

    // Unreachable
    return 0;
}
