#include "project.h"

void producer(void)
{
    int r;
    soi_item_t tmp;

    while (1)
    {
        tmp = rand() % 128;

        r = sem_wait(bufinfo->s_empty);
        if (r != 0) break;

        // Critical section
        {
            r = sem_wait(bufinfo->s_access);
            if (r != 0) break;

            buffer[(bufinfo->position + bufinfo->size) % bufinfo->capacity] = tmp;
            ++bufinfo->size;

            r = sem_post(bufinfo->s_access);
            if (r != 0) break;
        }

        r = sem_post(bufinfo->s_filled);
        if (r != 0) break;

        fprintf(stdout, "produced element %u\n", tmp);
        sleep(rand() % 3);
    }

    // Error
    fprintf(stderr, "internal error\n");
    return;
}
