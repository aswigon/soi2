#ifndef SOI_PROJECT_H
#define SOI_PROJECT_H

#define _ISOC99_SOURCE 1
#define _POSIX_C_SOURCE 200112L

#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <semaphore.h>
#include <signal.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#define N 8

#define READ1 0x1
#define READ2 0x2

typedef struct soi_bufinfo
{
    sem_t *s_access;
    sem_t *s_filled;
    sem_t *s_empty;
    sem_t *s_read;
    sem_t *s_mark1;
    sem_t *s_mark2;
    size_t size;
    size_t capacity;
    size_t position;
    size_t read_mask;
}
soi_bufinfo_t;

typedef unsigned int soi_item_t;

void producer(void);
void consumer(void);
void reader1(void);
void reader2(void);

extern soi_bufinfo_t *bufinfo;
extern soi_item_t *buffer;

#endif /* !SOI_PROJECT_H */
