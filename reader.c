#include "project.h"

void reader1(void)
{
    int r;
    soi_item_t tmp;

    while (1)
    {
        r = sem_wait(bufinfo->s_filled);
        if (r != 0) break;

        r = sem_wait(bufinfo->s_mark1);
        if (r != 0) break;

        // Critical section
        {
            r = sem_wait(bufinfo->s_access);
            if (r != 0) break;

            tmp = buffer[bufinfo->position];
            bufinfo->read_mask |= READ1;

            r = sem_post(bufinfo->s_access);
            if (r != 0) break;
        }

        r = sem_post(bufinfo->s_read);
        if (r != 0) break;

        r = sem_post(bufinfo->s_filled);
        if (r != 0) break;

        fprintf(stderr, "read1 element %u\n", tmp);
        sleep(rand() % 2);
    }

    // Error
    fprintf(stderr, "internal error\n");
    return;
}

void reader2(void)
{
    int r;
    soi_item_t tmp;

    while (1)
    {
        r = sem_wait(bufinfo->s_filled);
        if (r != 0) break;

        r = sem_wait(bufinfo->s_mark2);
        if (r != 0) break;

        // Critical section
        {
            r = sem_wait(bufinfo->s_access);
            if (r != 0) break;

            tmp = buffer[bufinfo->position];
            bufinfo->read_mask |= READ2;

            r = sem_post(bufinfo->s_access);
            if (r != 0) break;
        }

        r = sem_post(bufinfo->s_read);
        if (r != 0) break;

        r = sem_post(bufinfo->s_filled);
        if (r != 0) break;

        fprintf(stderr, "read2 element %u\n", tmp);
        sleep(rand() % 4);
    }

    // Error
    fprintf(stderr, "internal error\n");
    return;
}
