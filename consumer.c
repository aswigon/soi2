#include "project.h"

void consumer(void)
{
    int r;
    soi_item_t tmp, tmp2;
    int another;
    int post_m1, post_m2;

    while (1)
    {
        post_m1 = post_m2 = 0;
        another = 0;
        tmp = tmp2 = 0;

        r = sem_wait(bufinfo->s_read);
        if (r != 0) break;

        r = sem_wait(bufinfo->s_filled);
        if (r != 0) break;

        // Critical section
        {
            r = sem_wait(bufinfo->s_access);
            if (r != 0) break;

            tmp = buffer[bufinfo->position];
            --bufinfo->size;
            ++bufinfo->position;
            bufinfo->position %= bufinfo->capacity;

            if (bufinfo->read_mask & READ1) post_m1 = 1;
            if (bufinfo->read_mask & READ2) post_m2 = 1;

            bufinfo->read_mask = 0;

            r = sem_post(bufinfo->s_access);
            if (r != 0) break;
        }

        r = sem_trywait(bufinfo->s_read);
        if (r != 0 && errno == EAGAIN) /*nothing*/;
        else if (r != 0) break;
        else another = 1;

        r = sem_post(bufinfo->s_empty);
        if (r != 0) break;

        // Maybe consume another element
        if (another)
        {
            r = sem_wait(bufinfo->s_filled);
            if (r != 0) break;

            // Critical section
            {
                r = sem_wait(bufinfo->s_access);
                if (r != 0) break;

                tmp2 = buffer[bufinfo->position];
                --bufinfo->size;
                ++bufinfo->position;
                bufinfo->position %= bufinfo->capacity;

                r = sem_post(bufinfo->s_access);
                if (r != 0) break;
            }

            r = sem_post(bufinfo->s_empty);
            if (r != 0) break;
        }

        if (post_m1)
        {
            r = sem_post(bufinfo->s_mark1);
            if (r != 0) break;
        }

        if (post_m2)
        {
            r = sem_post(bufinfo->s_mark2);
            if (r != 0) break;
        }

        fprintf(stderr, "consumed element %u\n", tmp);
        if (another) fprintf(stderr, "consumed2 element %u\n", tmp2);
        sleep(rand() % 4);
    }

    // Error
    fprintf(stderr, "internal error\n");
    return;
}
